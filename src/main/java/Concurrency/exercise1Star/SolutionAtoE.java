/*
1. *You are the manager. You have 5 employees. Simulate the situation in which each of them comes at a different time to work.
a) Every employee, after getting to work, displays the information „<name: I came to work at <time HH:MM>.”
b) Every 10 seconds, the employee displays „name: I’m still working!”
c) Every 30 seconds, we release one of the employees to home (remember about stopping the thread!) and remove the employee from the „active employees list”
d) When you release your employee to home, print „<name: <time HH:MM>, it's time to go home!”
e) * When you release a given employee, all of the others speed up. From that moment, display the information about work 2 seconds faster.
 */
package Concurrency.exercise1Star;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SolutionAtoE {
    public static void main(String[] args) throws InterruptedException {
        List<Thread> activeEmployeeThreads = new ArrayList<>();

        Employee ivo = new Employee("Ivo");
        employeeStartsWorkday(activeEmployeeThreads, ivo);

        Employee ance = new Employee("Ance");
        employeeStartsWorkday(activeEmployeeThreads, ance);

        Employee kate = new Employee("Kate");
        employeeStartsWorkday(activeEmployeeThreads, kate);

        while (!activeEmployeeThreads.isEmpty()) {
            Thread.sleep(30000);
            if (activeEmployeeThreads.size() > 1) {
                int randomIndex = getRandomNumberInRange(0, activeEmployeeThreads.size() - 1);
                sendEmployeeHome(activeEmployeeThreads, randomIndex);
            } else {
                sendEmployeeHome(activeEmployeeThreads, 0);
            }
        }
    }

    private static void employeeStartsWorkday(List<Thread> workingEmployees, Employee employee) {
        Thread employeeThread = new Thread(employee);
        employeeThread.start();
        workingEmployees.add(employeeThread);
    }

    private static void sendEmployeeHome(List<Thread> workingEmployees, int randomIndex) {
        workingEmployees.get(randomIndex).interrupt();
        workingEmployees.remove(randomIndex);
        Employee.setWorkSpeed(2000);
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
