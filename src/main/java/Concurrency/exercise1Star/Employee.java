package Concurrency.exercise1Star;

import java.sql.Timestamp;

class Employee implements Runnable {
    static int workSpeed = 10000;
    private String name;

    public Employee(String name) {
        this.name = name;
    }

    public static void setWorkSpeed(int workSpeed) {
        Employee.workSpeed -= workSpeed;
    }

    public String getName() {
        return name;
    }

    void arriveToWork() {
        System.out.printf("\n%s: I came to work at %s.", name, timeStamp());
    }

    void printWorkingStatus() {
        System.out.printf("\n%s: I’m still working!", name);
    }

    void goHome() {
        System.out.printf("\n%s: %s, it's time to go home!", name, timeStamp());

    }

    private String timeStamp() {
        return String.valueOf(new Timestamp(System.currentTimeMillis())).substring(11, 16);
    }

    @Override
    public void run() {
        arriveToWork();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(workSpeed);
                printWorkingStatus();
            } catch (InterruptedException e) {
                break;
            }
        }
        goHome();
    }
}
