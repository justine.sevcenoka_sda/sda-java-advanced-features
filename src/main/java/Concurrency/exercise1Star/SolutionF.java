/*
1. *You are the manager. You have 5 employees. Simulate the situation in which each of them comes at a different time to work.
f) ** The manager decides in which order release employees (e.g. through an earlier defined list)
 */
package Concurrency.exercise1Star;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SolutionF {
    private static List<Thread> activeEmployeeThreads;
    private static List<Employee> workReleaseList;

    public static void main(String[] args) throws InterruptedException {
        activeEmployeeThreads = new ArrayList<>();

        Employee ivo = new Employee("Ivo");
        employeeStartsWorkday(activeEmployeeThreads, ivo);

        Employee ance = new Employee("Ance");
        employeeStartsWorkday(activeEmployeeThreads, ance);

        Employee kate = new Employee("Kate");
        employeeStartsWorkday(activeEmployeeThreads, kate);

        workReleaseList = Arrays.asList(ance, ivo, kate);

        for (Employee employee : workReleaseList) {
            Thread.sleep(30000);
            sendEmployeeHome(employee);
        }
    }

    private static void employeeStartsWorkday(List<Thread> workingEmployees, Employee employee) {
        Thread employeeThread = new Thread(employee);
        employeeThread.start();
        employeeThread.setName(employee.getName());
        workingEmployees.add(employeeThread);
    }

    private static void sendEmployeeHome(Employee employee) {
        int indexOfEmployee = 0;

        for (int i = 0; i < activeEmployeeThreads.size(); i++) {
            if (activeEmployeeThreads.get(i).getName().equals(employee.getName())) {
                indexOfEmployee = i;
                break;
            }
        }

        activeEmployeeThreads.get(indexOfEmployee).interrupt();
        activeEmployeeThreads.remove(indexOfEmployee);
        Employee.setWorkSpeed(2000);
    }
}
