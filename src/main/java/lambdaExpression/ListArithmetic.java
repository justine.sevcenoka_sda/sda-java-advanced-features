package lambdaExpression;

import java.util.List;

interface ListArithmetic {
    int operation(List<Integer> list);
}
