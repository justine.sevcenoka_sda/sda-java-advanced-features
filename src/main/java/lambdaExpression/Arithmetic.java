package lambdaExpression;

interface Arithmetic<T> {
    T operation(T a, T b);
}
