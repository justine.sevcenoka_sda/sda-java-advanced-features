package lambdaExpression;

import java.util.List;

interface WordCounter {
    int countWords(List<String> list);
}
