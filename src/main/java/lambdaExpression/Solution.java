//1. Create and present the usage of lambda expressions:

package lambdaExpression;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

class Solution {
    public static void main(String[] args) {
//1.a) Addition, subtraction, multiplication, division.
        Arithmetic<Integer> addition = Integer::sum;
        System.out.println(addition.operation(10, 1)); //prints 11

        BiFunction<Integer, Integer, Integer> subtraction = (a, b) -> (a - b);
        System.out.println(subtraction.apply(3, 5)); // prints -2

        Arithmetic<Integer> multiplication = (a, b) -> (a * b);
        System.out.println(multiplication.operation(2, 7)); //prints 14

        Arithmetic<Integer> division = (a, b) -> (a / b);
        System.out.println(division.operation(21, 3)); // prints 7
//1.b) The sum of elements (int type) of the list.
        ListArithmetic sumList = l -> {
            int sum = 0;
            for (Integer integer : l) {
                sum += integer;
            }
            return sum;
        };

        Function<List<Integer>, Integer> intsum = arr -> {
            int sum = 0;
            for (Integer integer : arr) {
                sum += integer;
            }
            return sum;
        };

        System.out.println(sumList.operation(Arrays.asList(5, 6, 7))); //prints 18
        System.out.println();
//1.c) Number of words in the input expression (list containing elements of type String).
        WordCounter someText = t -> {
            int wordCount = 0;
            for (String s : t) {
                String[] stringArray = s.split(" ");
                wordCount += stringArray.length;
            }
            return wordCount;
        };
        System.out.println(someText.countWords(Arrays.asList("This is my short", "text", "with a question mark at the end?")));
        //prints 12
//1.d) * List before and after sorting (use the Arrays class and lambda expressions:
// String :: compareToIgnoreCase as a comparator)
        List<String> listToBeSorted = Arrays.asList("This", "orange", "apple");
        System.out.println(listToBeSorted.toString());
        listToBeSorted.sort(String::compareToIgnoreCase);
        System.out.println(listToBeSorted.toString()); //prints sorted alphabetically: apple, orange, This
    }

}
