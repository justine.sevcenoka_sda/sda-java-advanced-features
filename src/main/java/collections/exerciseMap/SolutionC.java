/*
Create a map and display its result (data should be provided by the user - console):
c) Names and lists of friends (other names).
 */
package collections.exerciseMap;

import java.util.*;

class SolutionC {
    public static void main(String[] args) {
        Map<String, List<String>> nameAndFriends = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                String input = sc.nextLine();
                if (input.isEmpty()) {
                    break;
                } else {
                    String[] splitInput = input.split(" ");

                    List<String> friends = new ArrayList<>();
                    for (int i = 1; i < splitInput.length; i++) {
                        friends.add(splitInput[i]);
                    }

                    nameAndFriends.put(splitInput[0], friends);
                }
            }
            for (String name : nameAndFriends.keySet()) {
                System.out.println("Name: " + name + " Friends: " + nameAndFriends.get(name));
            }
        } catch (Exception e) {
            e.getMessage();
        }

    }
}
