/*
Create a map and display its result (data should be provided by the user - console):
d) * Names and details (map of maps)
 */
package collections.exerciseMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class SolutionD {
    public static void main(String[] args) {
        Map<String, Map<String, String>> keyMap = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            printStartInstructions();
            Boolean addNewPerson = true;
            while (addNewPerson) {
                String input = getNameFromIO(sc);
                if (input.isEmpty()) {
                    addNewPerson = false;
                } else {
                    printDetailInstructions();
                    Map<String, String> detailMap = new HashMap<>();
                    Boolean addNewDetails = true;
                    while (addNewDetails) {
                        String detailInput = sc.nextLine();
                        if (detailInput.equals("q")) {
                            addNewDetails = false;
                        } else if (detailInput.isEmpty()) {
                            addNewDetails = false;
                            addNewPerson = false;
                        } else {
                            addDetails(keyMap, input, detailMap, detailInput);
                        }
                    }
                }
            }
            printDetails(keyMap);
        } catch (Exception e) {
            e.getMessage();
        }

    }

    private static void printDetailInstructions() {
        System.out.println("Write the details following this template: [key] [value]");
    }

    private static void printStartInstructions() {
        System.out.println("Press [q] to switch to next person\nPress [enter] to print all keymaps");
    }

    private static String getNameFromIO(Scanner sc) {
        System.out.print("Write the name: ");
        return sc.nextLine();
    }

    private static void addDetails(Map<String, Map<String, String>> nameAndDetails, String input, Map<String, String> detailMap, String detailInput) {
        String[] splitInput = detailInput.split(" ");
        detailMap.put(splitInput[0], splitInput[1]);
        nameAndDetails.put(input, detailMap);
    }

    private static void printDetails(Map<String, Map<String, String>> nameAndDetails) {

        for (String name : nameAndDetails.keySet()) {
            final Map<String, String> properties = nameAndDetails.get(name);
            System.out.printf("\n" + name.toUpperCase() + "\n");

            for (String detail : properties.keySet()) {
                System.out.printf("%s: %s\n", detail, properties.get(detail));
            }
        }
    }
}
