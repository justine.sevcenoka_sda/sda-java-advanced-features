/*
Create a map and display its result (data should be provided by the user - console):
b) Names and ages.
 */
package collections.exerciseMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class SolutionB {
    public static void main(String[] args) {
        Map<String, Integer> nameAge = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                String input = sc.nextLine();
                if (input.isEmpty()) {
                    break;
                } else {
                    String[] splitInput = input.split(" ");
                    nameAge.put(splitInput[0], Integer.valueOf(splitInput[1]));
                }
            }
            printAges(nameAge);
        } catch (Exception e) {
            e.getMessage();
        }

    }

    private static void printAges(Map<String, Integer> nameAge) {
        for (String name : nameAge.keySet()) {
            System.out.println("Name: " + name + " Age: " + nameAge.get(name));
        }
    }
}
