/*
Create a map and display its result (data should be provided by the user - console):
a) Names and surnames
 */
package collections.exerciseMap;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class SolutionA {
    public static void main(String[] args) {
        Map<String, String> nameSurname = new HashMap<>();

        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                String input = sc.nextLine();
                if (input.isEmpty()) {
                    break;
                } else {
                    String[] splitInput = input.split(" ");
                    nameSurname.put(splitInput[0], splitInput[1]);
                }
            }
            printSurnames(nameSurname);
        } catch (Exception e) {
            e.getMessage();
        }

    }

    private static void printSurnames(Map<String, String> nameSurname) {
        for (String i : nameSurname.keySet()) {
            System.out.println("Name: " + i + " Surname: " + nameSurname.get(i));
        }
    }
}
