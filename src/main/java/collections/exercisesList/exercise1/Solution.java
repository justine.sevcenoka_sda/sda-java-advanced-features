/*
Create a List and display its result (data should be provided by the user - console):
a) Purchases to be made. *If an element already exists on the list, then it should not be added.
b) *Add to the example above the possibility of "deleting" purchased elements
c) Display only those purchases that start with „m” (e.g. milk)
d) * View only purchases whose next product on the list starts with „m” (e.g. eggs, if milk was next on the list)
 */

package collections.exercisesList.exercise1;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Purchases purchases = new Purchases();
        try (Scanner sc = new Scanner(System.in)) {

            System.out.println("AVAILABLE COMMANDS\nadd: to add product\nremove: to remove product\nlistm: to display purchases that start with 'm'\nlistnextm: to display purchases whose next product on the list starts with 'm'\nquit: to quit the program\n[enter]: to display full list\n\nEXAMPLES\nadd milk\nremove bread\nremove 1\nlistm\nlistnextm\nquit");

            while (true) {
                String input = sc.nextLine();
                if (input.toLowerCase().equals("quit")) {
                    break;
                } else if (input.toLowerCase().startsWith("add ")) {
                    purchases.add(input);
                } else if (input.toLowerCase().startsWith("remove ")) {
                    purchases.remove(input);
                } else if (input.toLowerCase().startsWith("listm")) {
                    purchases.printListM();
                } else if (input.toLowerCase().startsWith("listnextm")) {
                    purchases.printListNextM();
                } else if (input.isEmpty()) {
                    purchases.printList();
                } else {
                    System.err.println("Invalid command");
                }
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
