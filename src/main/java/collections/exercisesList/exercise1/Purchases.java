package collections.exercisesList.exercise1;

import java.util.ArrayList;
import java.util.List;

class Purchases {

    private List<String> purchases;

    public Purchases() {
        this.purchases = new ArrayList<>();
    }

    void add(String input) {
        String newPurchase = input.replaceFirst("add ", "");
        if (!purchases.contains(newPurchase)) {
            purchases.add(newPurchase);
        } else {
            System.out.println("Product already exists in the list");
        }
    }

    void remove(String input) {
        String newPurchase = input.replaceFirst("remove ", "");
        if (newPurchase.matches("\\d+")) {
            int index = Integer.valueOf(newPurchase);
            purchases.remove(index);
        } else {
            purchases.remove(newPurchase);
        }
    }

    void printList() {
        for (String purchase : purchases) {
            System.out.println(purchase);
        }
    }

    void printListM() {
        for (String purchase : purchases) {
            if (purchase.startsWith("m")) {
                System.out.println(purchase);
            }
        }
    }

    void printListNextM() {
        for (int i = 0; i < purchases.size() - 1; i++) {
            if (purchases.get(i + 1).startsWith("m") && !purchases.get(i).startsWith("m")) {
                System.out.println(purchases.get(i));
            }
        }
    }

}
