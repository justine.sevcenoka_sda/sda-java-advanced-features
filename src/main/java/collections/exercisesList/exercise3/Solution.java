package collections.exercisesList.exercise3;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        List<List<Integer>> multiplicationTable = new ArrayList<>(10);

        final int MULTIPLICATION_TABLE_SIZE = 10;
        for (int i = 1; i <= MULTIPLICATION_TABLE_SIZE; i++) {
            List<Integer> integers = new ArrayList<>(10);
            for (int k = 1; k <= 10; k++) {
                integers.add(i * k);
            }
            multiplicationTable.add(integers);
        }

        for (List<Integer> integers : multiplicationTable) {
            System.out.println(integers.toString());
        }
    }
}
