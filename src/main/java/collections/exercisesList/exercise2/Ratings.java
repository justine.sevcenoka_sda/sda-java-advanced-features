package collections.exercisesList.exercise2;

import java.util.ArrayList;
import java.util.List;

class Ratings {
    private static final int LOWER_BOUND = 1;
    private static final int UPPER_BOUND = 6;
    private List<Integer> ratings;

    Ratings() {
        ratings = new ArrayList<>();
    }

    void add(int rating) throws InvalidRatingException {
        if (isValid(rating)) {
            ratings.add(rating);
        } else
            throw new InvalidRatingException(String.format("Rating must be in range %d-%d (inclusive).", LOWER_BOUND, UPPER_BOUND));
    }

    static boolean isValid(int rating) {
        return rating >= LOWER_BOUND && rating <= UPPER_BOUND;
    }

    double getAverage() {
        double sum = 0;
        int size = ratings.size();

        for (Integer rating : this.ratings) {
            sum += rating;
        }
        return sum / size;
    }
}
