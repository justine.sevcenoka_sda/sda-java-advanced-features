/*
Ratings received. Display their average. The numbers can not be less than 1 and greater than 6.
 */
package collections.exercisesList.exercise2;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws InvalidRatingException {
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Input your ratings or press enter to finish and calculate average.");
            Ratings ratings = new Ratings();
            while (true) {
                String input = sc.nextLine();
                if (input.isEmpty()) {
                    break;
                } else {
                    ratings.add(Integer.parseInt(input));
                }

            }

            System.out.println(ratings.getAverage());
        } catch (Error e) {
            e.getMessage();
        }
    }

}
