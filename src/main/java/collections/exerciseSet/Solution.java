/*
1. Create a set consisting of colors - given from the user.
2. Present the removal of individual elements from the set.
3. Display the collection before and after sorting.
 */

package collections.exerciseSet;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Solution {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {

            Set<String> colors = new HashSet<>();
            System.out.println("Create your set of colors.\nAVAILABLE COMMANDS\n[color]: to add any color to the set\nremove [color]: to remove it from the list.\nset: to display unsorted set\nsortset: to display sorted set\n[enter]: to exit the program");

            while (true) {
                String input = sc.nextLine().toLowerCase();
                if (input.isEmpty()) {
                    break;
                } else if (input.startsWith("remove")) {
                    String colorToRemove = input.replaceFirst("remove ", "");
                    colors.remove(colorToRemove);
                    System.out.println(String.format("%s removed from the set", colorToRemove));
                } else if (input.equals("set")) {
                    System.out.println("Your set of colors:");
                    for (String color : colors) {
                        System.out.println(color);
                    }
                } else if (input.equals("sortset")) {
                    Set<String> sortedColors = new TreeSet<>(colors);
                    System.out.println("Sorted set of colors:");
                    for (String sortedColor : sortedColors) {
                        System.out.println(sortedColor);
                    }
                } else {
                    colors.add(input);
                }
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
