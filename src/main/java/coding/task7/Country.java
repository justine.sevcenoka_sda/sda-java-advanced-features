package coding.task7;

import java.util.ArrayList;
import java.util.List;

public class Country {
    private String name;
    private List<String> cities = new ArrayList<String>();

    public Country(String name, List<String> cities) {
        this.name = name;
        this.cities = cities;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public List<String> getCities() {
        return cities;
    }
}
