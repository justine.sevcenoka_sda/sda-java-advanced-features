package coding.task7;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        List<Country> countryList = new ArrayList<>();
        fakeUserInput(countryList);
        //      readUserInput(countryList);
        printCreatedPlan(countryList);
        storeAsJsonFile(countryList);
        readAndDisplayJsonFile("countries.json");


    }

    private static void readAndDisplayJsonFile(String fileName) {
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(fileName)) {
            Object obj = jsonParser.parse(reader);
            JSONArray jsonCountryList = (JSONArray) obj;
            System.out.println(jsonCountryList.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void storeAsJsonFile(List<Country> countryList) {
        JSONArray jsonArray = new JSONArray();
        for (Country country : countryList) {
            JSONObject jsonCountry = new JSONObject();
            jsonCountry.put("name", country.getName());
            jsonCountry.put("cities", country.getCities());
            jsonArray.add(jsonCountry);
        }

        try (FileWriter file = new FileWriter("countries.json")) {
            file.write(jsonArray.toJSONString());
            file.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void fakeUserInput(List<Country> countryList) {
        List<String> latvianCities = new ArrayList<>();
        latvianCities.add("Riga");
        latvianCities.add("Liepaja");
        latvianCities.add("Riga");
        latvianCities.add("Ludza");
        Country latvia = new Country("Latvia", latvianCities);
        countryList.add(latvia);

        List<String> lithuanianCities = new ArrayList<>();
        lithuanianCities.add("Vilnius");
        lithuanianCities.add("Kaunas");
        lithuanianCities.add("Siauliai");
        lithuanianCities.add("Kaunas");
        Country lithuania = new Country("Lithuania", lithuanianCities);
        countryList.add(lithuania);


    }

    private static void readUserInput(List<Country> countryList) {
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter a country: ");
            String countryName = sc.nextLine();
            if (countryName.isEmpty()) {
                break;
            } else {
                List<String> cityList = new ArrayList<String>();
                while (true) {
                    System.out.print("Enter a city: ");
                    String cityName = sc.nextLine();
                    if (cityName.isEmpty()) {
                        countryList.add(new Country(countryName, cityList));
                        break;
                    } else {
                        cityList.add(cityName);
                    }
                }
            }
        }
    }

    private static void printCreatedPlan(List<Country> countryList) {
        for (Country country : countryList) {
            System.out.print("Visit " + country.getName().toUpperCase() + ":");
            for (int i = 0; i < country.getCities().size(); i++) {
                List<String> previousCities = country.getCities().subList(0, i);
                String currentCity = country.getCities().get(i);
                if (previousCities.contains(currentCity)) {
                    System.out.print(" back");
                }
                System.out.print(" to " + currentCity);

            }

            System.out.println("\n");
        }
    }
}
