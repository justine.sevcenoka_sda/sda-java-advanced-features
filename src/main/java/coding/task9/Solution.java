package coding.task9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

class Solution {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("ebook.txt");
        TxtFileStatistics ebookStatistics = new TxtFileStatistics(Files.readAllLines(path));

        ebookStatistics.printTop10WordOccurences();
        ebookStatistics.printJsonReport();

        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter a for for which you would like to see occurrence: ");
            String word = sc.nextLine();
            if (word.isEmpty()) {
                break;
            }
            System.out.println(ebookStatistics.findOccurrences(word) + " occurrences");
        }
    }
}
