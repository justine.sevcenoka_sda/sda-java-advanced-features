package coding.task9;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TxtFileStatistics {
    List<String> fileLines;
    private int totalWords;
    private Map<String, Integer> wordOccurrences;
    private int numberOfNumbers;
    private int numberOfSymbols;
    private int numberOfLines;
    private int averageLineLength;
    private int maxLineLength;
    private int minLineLength;
    private int numberOfSentences;
    private LinkedHashMap<String, Integer> topTenOccurrences;


    public TxtFileStatistics(List<String> fileLines) {
        this.fileLines = fileLines;
        wordOccurrences = new HashMap<>();
        calculateNumberOfLines();
        calculateMaxLineLength();
        calculateNumberOfNumbers();
        calculateWordOccurrences();
        calculateAverageLineLength();
        calculateNumberOfNumbers();
        calculateNumberOfSymbols();
        calculateMinLineLength();
        getTopTenOccurrences();
        calculateNumberOfSentences();
    }

    private JSONObject createJsonReport() {
        JSONObject jsonReport = new JSONObject();
        jsonReport.put("Number of total words", totalWords);
        jsonReport.put("Number of symbols", numberOfSymbols);
        jsonReport.put("Number of numbers", numberOfNumbers);
        jsonReport.put("Average line length", averageLineLength);
        jsonReport.put("Min line length", minLineLength);
        jsonReport.put("Max line length", maxLineLength);
        jsonReport.put("Number of lines", numberOfLines);
        jsonReport.put("10 most frequent words", topTenOccurrences.keySet());
        jsonReport.put("Number of sentences", numberOfSentences);
        return jsonReport;
    }

    void printJsonReport() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(createJsonReport().toJSONString());
        String prettyJsonString = gson.toJson(je);
        System.out.println("\nJSON REPORT");
        System.out.println(prettyJsonString);
    }

    private void calculateNumberOfSentences() {
        String totalText = "";
        for (String fileLine : fileLines) {
            totalText += fileLine;
        }
        String[] totalSentences = totalText.split("\\.");
        numberOfSentences = totalSentences.length;
    }

    private void calculateMaxLineLength() {
        for (String fileLine : fileLines) {
            if (fileLine.length() > maxLineLength) {
                maxLineLength = fileLine.length();
            }
        }
    }

    private void calculateNumberOfLines() {
        numberOfLines = fileLines.size();
    }

    private void calculateAverageLineLength() {
        int totalLineLength = 0;
        for (String fileLine : fileLines) {
            totalLineLength += fileLine.length();
        }
        averageLineLength = totalLineLength / numberOfLines;
    }

    void calculateWordOccurrences() {
        for (String fileLine : fileLines) {
            String[] wordsInLine = fileLine.split(" ");
            for (String word : wordsInLine) {
                word = word.toLowerCase().replaceAll("[^a-zA-Z]", "");
                addWordToOccurrences(word);
            }
        }
    }

    private void calculateNumberOfNumbers() {
        for (String fileLine : fileLines) {
            fileLine = fileLine.replaceAll("[^0-9 ]", "");
            String[] fileLineSplitted = fileLine.split(" ");
            numberOfNumbers += fileLineSplitted.length;
        }
    }

    private void calculateNumberOfSymbols() {
        for (String fileLine : fileLines) {
            fileLine = fileLine.replaceAll(" ", "");
            numberOfSymbols += fileLine.length();
        }
    }

    private void calculateMinLineLength() {
        minLineLength = fileLines.get(1).length();
        for (String fileLine : fileLines) {
            if (fileLine.length() < minLineLength) {
                minLineLength = fileLine.length();
            }
        }
    }

    private void getTopTenOccurrences() {
        topTenOccurrences = wordOccurrences.entrySet()
                .stream()
                .sorted(((o1, o2) -> o2.getValue().compareTo(o1.getValue())))
                .limit(10)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    void printTop10WordOccurences() {
        System.out.println("TOP 10 WORD OCCURRENCES");
        for (Map.Entry<String, Integer> stringIntegerEntry : topTenOccurrences.entrySet()) {
            System.out.println(stringIntegerEntry.toString());
        }
    }

    private void addWordToOccurrences(String word) {
        if (!word.equals("")) {
            totalWords++;
            if (wordOccurrences.containsKey(word)) {
                int current = wordOccurrences.get(word);
                wordOccurrences.put(word, ++current);
            } else {
                wordOccurrences.put(word, 1);
            }
        }
    }

    int findOccurrences(String word) {
        return wordOccurrences.get(word);
    }
}
