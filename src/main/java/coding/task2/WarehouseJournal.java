package coding.task2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

class WarehouseJournal {
    List<Item> itemCatalogue = new LinkedList<>();
    Map<String, Integer> itemCount = new HashMap<>();

    void addToList(Item item) {
        if (!itemCatalogue.contains(item)) {
            itemCatalogue.add(item);
        }
        updateItemCount(item.getSku(), 1);
    }

    void addToList(String sku, int count) {
        itemCatalogue.stream()
                .filter(item -> item.getSku().equals(sku))
                .findFirst()
                .ifPresent(item -> updateItemCount(sku, count));

//        for (Item item : itemCatalogue) {
//            if (item.getSku().equals(sku)) {
//                addToItemCount(sku, count);
//                break;
//            }
//        }
    }

    void updateItemCount(String sku, int count) {
        if (itemCount.containsKey(sku)) {
            int currentCount = itemCount.get(sku);
            count += currentCount;
        }
        itemCount.put(sku, count);

    }


    void deleteItem(Item item) {
        itemCatalogue.remove(item);
        System.err.println(item.getName() + " was deleted");
    }

    void displayAllItems() {
        System.out.println("LIST OF ITEMS: ");

        for (int i = 0; i < itemCatalogue.size(); i++) {
            int count = itemCount.get(itemCatalogue.get(i).getSku());
            System.out.printf("[%d] %d x %s\n", i, count, itemCatalogue.get(i));
        }
    }

    void displaySpecificItems(String itemType) {
        System.out.printf("LIST OF %ss: \n", itemType.toUpperCase());

        for (Item item : itemCatalogue) {
            if (itemType.toLowerCase().equals(item.getClass().getSimpleName().toLowerCase())) {
                System.out.println(item.getName());
            }
        }
    }

    Item getItem(int index) throws ItemNotFoundException {
        if (index < itemCatalogue.size()) {
            return itemCatalogue.get(index);
        } else throw new ItemNotFoundException(index);
    }

    double calculateTotalValueBeforeTax() {
        double sum = 0;
        for (Item item : itemCatalogue) {
            sum += item.getPriceBeforeTax();
        }
        return sum;
    }

}
