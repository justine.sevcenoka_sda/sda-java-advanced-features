package coding.task2;

enum Country {
    LATVIA(0.21),
    UK(0.20),
    ITALY(0.22),
    GERMANY(0.19);

    private double vat;


    Country(double vat) {
        this.vat = vat;
    }

    double getVatRate() {
        return vat;
    }
}
