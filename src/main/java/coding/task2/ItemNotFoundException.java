package coding.task2;

public class ItemNotFoundException extends Exception {
    public ItemNotFoundException(int index) {
        super(String.format("Item with index %d not found", index));
    }
}
