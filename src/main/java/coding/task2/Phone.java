package coding.task2;

class Phone extends Item {
    int productionYear;

    Phone(String name, String sku, double priceBeforeTax, int productionYear) {
        super(name, sku, priceBeforeTax);
        this.productionYear = productionYear;
    }


    @Override
    void updatePrivateDetails(String property, String value) {
        if ("productionYear".equals(property)) {
            productionYear = Integer.parseInt(value);
        }
    }
}
