package coding.task2;

public enum ChargerType {
    MICRO_USB,
    MINI_USB,
    USB_C,
    IOS_30PIN,
    IOS_8PIN

}
