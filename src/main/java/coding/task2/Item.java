package coding.task2;

abstract class Item {
    String name;
    String sku;
    double priceBeforeTax;
    double retailPrice;

    Item(String name, String sku, double priceBeforeTax) {
        this.name = name;
        this.sku = sku;
        this.priceBeforeTax = priceBeforeTax;
    }

    protected double calculateRetailPrice(Country country) {
        return priceBeforeTax * (1 + country.getVatRate());
    }

    String getName() {
        return name;
    }

    double getPriceBeforeTax() {
        return priceBeforeTax;
    }

    String getSku() {
        return sku;
    }

    abstract void updatePrivateDetails(String property, String value);

    void updateDetails(String property, String value) {
        if ("name".equals(property)) {
            name = value;
        } else if ("sku".equals(property)) {
            sku = value;
        } else if ("price".equals(property)) {
            priceBeforeTax = Double.valueOf(value);
        }
        updatePrivateDetails(property, value);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return sku.equals(item.sku);
    }

    @Override
    public int hashCode() {
        return sku.hashCode();
    }
}
