//todo Salabot, kapec ir 2 produkti, nevis 1
//todo Delete element - opcija izdzest vienu vai vairakus
//todo Salabot add to list
//todo Viss viena mapa - elements un daudzums
//todo getTotalValueForCategory - izmanto metodi get totalvalue un ieseto, ka based on category/item.
//Pec tam so getTotalValueForCategory izmanto uz getTotalValue(items)
//(category==null ? “” : category.getname) + value
//todo get totalvalue - lai aprekina nemot vera visu daudzumu (iteret cauri entry set 0 key&value). entry.getKey & entry.getValue

package coding.task2;

public class Solution {
    public static void main(String[] args) {
        WarehouseJournal phoneWarehouse = new WarehouseJournal();

        Phone iphone1 = new Phone("iPhone 11 Pro", "NI8342", 1199.99, 2020);
        phoneWarehouse.addToList(iphone1);
        Phone samsung1 = new Phone("Samsung", "sams983", 800, 2020);
        phoneWarehouse.addToList(samsung1);
//        phoneWarehouse.addToList("sams983", 5);

        phoneWarehouse.addToList(new Charger("Charger for iPhone", "NI8342", 24, ChargerType.IOS_8PIN));

        phoneWarehouse.displayAllItems();
        try {
            phoneWarehouse.getItem(0).updatePrivateDetails("name", "iPhone 8");
        } catch (ItemNotFoundException e) {
            System.err.println(e.getMessage());
        }

        phoneWarehouse.displayAllItems();
        phoneWarehouse.displaySpecificItems("phone");

        System.out.println(phoneWarehouse.calculateTotalValueBeforeTax());

        phoneWarehouse.deleteItem(iphone1);


    }
}
