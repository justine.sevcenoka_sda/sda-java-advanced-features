package coding.task2;

class Charger extends Item {
    private ChargerType type;

    Charger(String name, String sku, double priceBeforeTax, ChargerType type) {
        super(name, sku, priceBeforeTax);
        this.type = type;
    }


    @Override
    void updatePrivateDetails(String property, String value) {
        if ("type".equals(property)) {
            type = ChargerType.valueOf(value);
        }
    }
}
