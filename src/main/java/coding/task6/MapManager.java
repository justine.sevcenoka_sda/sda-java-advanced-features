package coding.task6;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MapManager {
    private static Map<String, String> stringStringMap = new HashMap<>();
    private static Map<Employee, Manager> employeeManagerMap = new HashMap<>();
    private static Map<Manager, List<Employee>> managerEmployeeListMap = new HashMap<>();

    public static Map<String, String> add(String key, String value) {
        stringStringMap.put(key, value);
        return stringStringMap;
    }

    public static Map<Employee, Manager> add(Employee key, Manager value) {
        employeeManagerMap.put(key, value);
        return employeeManagerMap;
    }

    public static Map<Manager, List<Employee>> add(Manager key, List<Employee> value) {
        managerEmployeeListMap.put(key, value);
        return managerEmployeeListMap;
    }

    public static void printAllMaps() {
        System.out.println("String String map");
        stringStringMap.forEach((key, value) -> System.out.print(key + " : " + value + "; "));
        System.out.print("\n");
        System.out.println("Employee Manager map");
        employeeManagerMap.forEach((key, value) -> System.out.print(key + " : " + value + "; "));
        System.out.print("\n");
        System.out.println("Manager List Employee map");
        managerEmployeeListMap.forEach((key, value) -> System.out.print(key + " : " + value + "; "));
    }

    public static Map<Employee, Manager> dismissEmployeeFromEmployeeManagerMap(String name) {
        employeeManagerMap.keySet()     // Set<Employee>
                .removeIf(employee -> name.equalsIgnoreCase(employee.getName()));
        return employeeManagerMap;
    }

    public static Map<Manager, List<Employee>> dismissEmployeeFromManagerEmployeeListMap(String name) {
        managerEmployeeListMap.values()         // Collection<List<Employee>>
                .forEach(employees -> employees.removeIf(employee -> employee.getName()
                        .equalsIgnoreCase(name)));
        return managerEmployeeListMap;

    }
}