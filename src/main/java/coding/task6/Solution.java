/*
Create a Maps where the key will be the employee and the value - his manager.

1. The key and value are of the String type
2. The key and value are classes of type Employee and Manager
3. The key is of type Manager, the value is a list storing the type Employee
4. For each map prove that they are working - add same keys and see that they do not duplicate.
5. Let the employee be dismissed, display the result
6. Allow to employ a new employee, display the result
 */
package coding.task6;

import java.util.ArrayList;
import java.util.Arrays;

class Solution {
    public static void main(String[] args) {

        MapManager.add("First key", "First value");
        MapManager.add("Second key", "Second value");
        MapManager.add("First key", "Third value");
        MapManager.add(new Employee("Martin"), new Manager("David"));
        MapManager.add(new Employee("Alexander"), new Manager("Michael"));
        MapManager.add(new Employee("Martin"), new Manager("Josh"));
        MapManager.add(new Manager("Otto"), new ArrayList<>(Arrays.asList(new Employee("Konstantin"), new Employee("John"))));
        MapManager.add(new Manager("Donald"), new ArrayList<>(Arrays.asList(new Employee("Peter"), new Employee("Mike"))));
        MapManager.add(new Manager("Otto"), new ArrayList<>(Arrays.asList(new Employee("Nicole"), new Employee("Brad"))));
        MapManager.printAllMaps();
        MapManager.dismissEmployeeFromEmployeeManagerMap("Alexander");
        MapManager.dismissEmployeeFromManagerEmployeeListMap("Nicole");
    }
}
