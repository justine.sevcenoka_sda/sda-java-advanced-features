package coding.task4;

enum CarBrandModel {
    DIABLO("Lamborghini"),
    P911("Porsche"),
    SUPRA("Toyota"),
    CIVIC_TYPE_R("Honda"),
    DEFENDER("Land Rover");

    private String brand;

    CarBrandModel(String brand) {
        this.brand = brand;
    }

    String getBrand() {
        return brand;
    }

}
