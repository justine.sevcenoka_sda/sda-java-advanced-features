package coding.task4;

class Car extends Vehicle {
    CarBrandModel brandModel;

    public Car(CarBrandModel brandModel) {
        this.brandModel = brandModel;
    }
}
