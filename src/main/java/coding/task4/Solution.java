//Inheritance and Enum
package coding.task4;

import java.util.Scanner;

class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the buyers information in the following format: [name] [surname] born [mm/dd/yyyy]");
        CharSequence buyersInfo = sc.nextLine();
        System.out.println(Parser.isNameSurname(buyersInfo));

    }
}
