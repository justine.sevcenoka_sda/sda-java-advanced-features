package coding.task4;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Parser {
    static Person parseBuyer(CharSequence buyerInfo) throws ParseException {
        if (isNameSurname(buyerInfo)) {
            Pattern p = Pattern.compile("\\s");
            String[] splitBuyersInfo = p.split(buyerInfo);
            Person person = new Person(splitBuyersInfo);
            return person;
        } else {
            return null;
        }
    }

    public static boolean isNameSurname(CharSequence buyersInfo) {
        Pattern p = Pattern.compile("(?:[A-Za-z]+ ){2}\\s*(born)\\s\\d+\\/\\d+\\/\\d{4}");
        Matcher m = p.matcher(buyersInfo);
        return m.matches();
    }

}
