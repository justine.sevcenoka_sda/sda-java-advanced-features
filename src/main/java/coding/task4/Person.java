package coding.task4;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

class Person {
    String name;
    String surname;
    Date birthDate;

    public Person(String name, String surname, Date birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Person(String[] args) throws ParseException {
        this.name = args[0];
        this.surname = args[1];
        DateFormat sourceFormat = new SimpleDateFormat("mm/dd/yyyy");
        this.birthDate = sourceFormat.parse(args[3]);
    }
}
