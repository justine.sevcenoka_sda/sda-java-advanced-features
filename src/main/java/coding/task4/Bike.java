package coding.task4;

class Bike extends Vehicle {
    BikeBrandModel brandModel;

    public Bike(BikeBrandModel brandModel) {
        this.brandModel = brandModel;
    }
}
