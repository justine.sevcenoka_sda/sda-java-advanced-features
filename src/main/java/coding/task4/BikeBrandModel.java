package coding.task4;

enum BikeBrandModel {
    SUPER_DUKE("KTM"),
    BAGHIRA("MZ"),
    R_NINE_T("BMW"),
    CIVIC_TYPE_R("Honda"),
    DEFENDER("Land Rover"),
    R1("Yamaha");

    private String brand;

    BikeBrandModel(String brand) {
        this.brand = brand;
    }

    String getBrand() {
        return brand;
    }

}
