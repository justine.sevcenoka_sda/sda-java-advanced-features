/*
Task 1 - Inheritance & Encapsulation
Create 4 classes: Person, Developer, JavaDeveloper, Gamer:
Person is a parent class; Developer inherits from Person, JavaDeveloper inherits from Developer
Person is a parent class for Gamer
What properties (fields) can you put for each of the classes
What methods (besides getters/setters) can you put into each class (no implementation - only names)
Create constructor for every class that will call constructor of the super class. Each constructor should display an information, that it has been called.
Create an object of type JavaDeveloper. What information will be displayed and in what order?
Using an object of type JavaDeveloper call a method that is defined in Developer class. What access modifier should it have?
Overload method from the Person class in JavaDeveloper class to accept additional parameters.
Override toString methods in each of classes. Use super.toString in subclasses.
todo List with programming languages
todo map with experience inyears for each language
todo JavaDeveloper - list with java versions (if knows Java 8, will know Java 6 as well)
 */
package coding.task1;

class Solution {
    public static void main(String[] args) {
        JavaDeveloper ivars = new JavaDeveloper("Ivars", "Kalnins", Gender.MALE,
                37, ProgrammingLanguage.JAVA, 9, 8);
        ivars.celebrateBirthday(ivars.getAge());
//         ivars.applyForJob();
        System.out.println(ivars.toString());
    }
}

