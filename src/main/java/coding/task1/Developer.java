package coding.task1;

class Developer extends Person {
    private ProgrammingLanguage language;
    private int experienceInYears;

    public Developer(String name, String surname, Gender gender, int age, ProgrammingLanguage programmingLanguage, int experienceInYears) {
        super(name, surname, gender, age);
        this.language = programmingLanguage;
        this.experienceInYears = experienceInYears;
        System.out.println(Developer.class.getSimpleName() + " constructor had been called");
    }

    protected void applyForJob(Job job) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String toString() {
        return super.toString() +
                " Developer{" +
                "language=" + language +
                ", experienceInYears=" + experienceInYears +
                '}';
    }
}