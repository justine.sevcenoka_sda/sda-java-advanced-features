package coding.task1;

class JavaDeveloper extends Developer {
    private int javaVersion;

    public JavaDeveloper(String name, String surname, Gender gender, int age, ProgrammingLanguage java, int experienceInYears, int javaVersion) {
        super(name, surname, gender, age, ProgrammingLanguage.JAVA, experienceInYears);
        this.javaVersion = javaVersion;
        System.out.println(JavaDeveloper.class.getSimpleName() + " constructor had been called");
    }

    private int upgradeJavaVersion(int javaVersion) {
        throw new RuntimeException("Not implemented");
    }

    private void celebrateBirthday(int age, int experienceInYears) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String toString() {
        return super.toString() +
                " JavaDeveloper{" +
                "javaVersion=" + javaVersion +
                '}';
    }
}
