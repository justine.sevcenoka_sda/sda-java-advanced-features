package coding.task1;

public class Gamer extends Person {
    private Game favouriteGame;

    public Gamer(String name, String surname, Gender gender, int age, Game favouriteGame) {
        super(name, surname, gender, age);
        this.favouriteGame = favouriteGame;
        System.out.println(Gamer.class.getSimpleName() + " constructor had been called");
    }

    private void playGame(Game game) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String toString() {
        return super.toString() +
                " Gamer{" +
                "favouriteGame=" + favouriteGame +
                '}';
    }
}
