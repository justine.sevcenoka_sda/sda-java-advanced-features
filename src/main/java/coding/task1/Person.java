package coding.task1;

class Person {
    private String name;
    private String surname;
    private Gender gender;
    private int age;

    public Person(String name, String surname, Gender gender, int age) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.age = age;
        System.out.println(Person.class.getSimpleName() + " constructor had been called");
    }

    public int getAge() {
        return age;
    }

    private void setAge(int age) {
        this.age = age;
    }

    protected void celebrateBirthday(int age) {
        this.setAge(++age);
    }


    @Override
    public String toString() {
        return " Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                '}';
    }
}

