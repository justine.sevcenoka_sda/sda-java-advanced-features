package coding.task1;

enum ProgrammingLanguage {
    JAVA,
    PHP,
    PASCAL,
    C,
    LISP,
    PYTHON,
    RUBY
}
