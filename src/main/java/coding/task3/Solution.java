package coding.task3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    static void main(String[] args) throws IOException {
        File csvFile = new File("personData.csv");

        List<String> csvLines = new BufferedReader(new FileReader(csvFile))
                .lines()
                .skip(1)
                .filter(s -> s.length() > 0)
                .collect(Collectors.toList());

        List<Person> personList = new ArrayList<>();

        for (String csvLine : csvLines) {
            String[] separatedLine = csvLine.split(",", -1);
            Person person = new Person(separatedLine);
            personList.add(person);
        }
        for (Person person : personList) {
            System.out.println(person.toString());
        }

    }
}
