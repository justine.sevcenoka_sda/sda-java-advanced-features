package coding.task3;

class Person {
    String name;
    String surname;
    String phoneNumber;
    String address;
    Integer birthYear;

    Person(String[] args) {
        this.name = args[0];
        this.surname = args[1];
        this.phoneNumber = args[2];
        this.address = args[3];
        if (!args[4].isEmpty()) {
            this.birthYear = Integer.parseInt(args[4]);
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", birthYear=" + birthYear +
                '}';
    }
}
