/*Create a simple Generic class, that will give a possibility, to store any kind of value within.
Add object of type String, Integer and Double to array of that Generic type.
Print all values of the array within a loop. */

package genericTypes.exercise2;

public class Solution {
    public static void main(String[] args) {

        Generic<Object> genericObject = new Generic<>();

        Object[] objectArray = new Object[3];
        objectArray[0] = "some string";
        objectArray[1] = new Integer(1);
        objectArray[2] = new Double(2.0);

        genericObject.setArray(objectArray);

        for (Object o : genericObject.getArray()) {
            System.out.println(o);
        }
    }


}
