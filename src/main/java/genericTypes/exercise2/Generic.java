package genericTypes.exercise2;

public class Generic<T> {
    private T[] array;

    public Generic() {
    }

    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
    }
}
