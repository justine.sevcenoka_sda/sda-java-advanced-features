/*create dynamic array implementation and implement:
1. add
2. remove by index
3. get by index
4. find by contents (not by index) - use equals method
5.  sort lexicographically (by result of toString method) - didn't do */

package genericTypes.additionalExercise;

import java.lang.reflect.Array;

public class MyArrayList<T> {
    T[] arr;
    int length;

    MyArrayList() {
        this.arr = (T[]) Array.newInstance(Object.class, length);
        this.length = 0;
    }

    T getByIndex(int index) {
        return arr[index];
    }

    void remove(int index) {
        this.length--;

        int jOld = 0;
        T[] newArr = (T[]) Array.newInstance(Object.class, this.length);
        for (int j = 0; j < this.length; j++) {
            if (j != index) {
                newArr[j] = this.arr[jOld];
            } else {
                jOld++;
                newArr[j] = this.arr[jOld];

            }
            jOld++;
        }
        this.arr = newArr;
    }

    void add(T o, int index) {
        this.length++;
        T[] newArr = (T[]) Array.newInstance(Object.class, this.length);
        for (int j = 0; j <= index; j++) {
            if (j != index) {
                newArr[j] = this.arr[j];
            } else if (j == index) {
                newArr[j] = o;
            }
        }

        for (int i = index + 1; i < this.length; i++) {
            int iOld = i - 1;
            newArr[i] = this.arr[iOld];
        }
        this.arr = newArr;
    }

    Integer getByContent(T o) {
        for (int i = 0; i < this.length; i++) {
            if (this.arr[i].equals(o)) {
                return i;
            }
        }
        return null;
    }
}
