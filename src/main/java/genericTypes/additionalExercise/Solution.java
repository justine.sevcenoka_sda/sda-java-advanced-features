package genericTypes.additionalExercise;

public class Solution {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();

        myArrayList.add("something", 0);
        myArrayList.add(3, 0);
        myArrayList.add(5.0, 0);
        myArrayList.add("abc", 2);

        System.out.println(myArrayList.getByIndex(0));
        System.out.println(myArrayList.getByIndex(1));
        System.out.println(myArrayList.getByIndex(2));

        myArrayList.remove(1);

        System.out.println(myArrayList.getByIndex(0));
        System.out.println(myArrayList.getByIndex(1));
        System.out.println(myArrayList.getByContent("something"));

    }
}
