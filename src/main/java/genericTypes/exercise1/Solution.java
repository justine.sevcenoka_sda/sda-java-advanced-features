package genericTypes.exercise1;

public class Solution {
    public static void main(String[] args) {
        Person shortPerson = new Person(154);
        Person averagePerson = new Person(180);
        Person tallPerson = new Person(194);

        System.out.println(shortPerson.compareTo(averagePerson));
        System.out.println(shortPerson.compareTo(tallPerson));
        System.out.println(tallPerson.compareTo(averagePerson));

    }


}
