package genericTypes.exercise1;

public class Person implements Comparable<Person> {
    private int heightInCm;

    public Person(int heightInCm) {
        this.heightInCm = heightInCm;
    }

    public int getHeightInCm() {
        return heightInCm;
    }

    @Override
    public int compareTo(Person o) {
        return this.getHeightInCm() - o.getHeightInCm();
    }
}
