/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
a) Count words.
 */
package inputOutput.exerciseWithNewIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Scanner;

public class SolutionA {

    public static void main(String[] args) {
        Path myFilePath = Paths.get("myNewFile.txt");

        try {
            LoremIpsum ipsum = new LoremIpsum();
            String fileParagraph = ipsum.getParagraphs(1);
            Files.write(myFilePath, Collections.singleton(fileParagraph));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Scanner sc = new Scanner(myFilePath)) {
            int wordCount = 0;
            while (sc.hasNext()) {
                sc.next();
                wordCount++;
            }
            System.out.println("wordCount = " + wordCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
