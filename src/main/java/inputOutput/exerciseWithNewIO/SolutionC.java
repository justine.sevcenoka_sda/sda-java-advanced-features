/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
*Select one word and print it’s number of occurences.
 */
package inputOutput.exerciseWithNewIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionC {

    public static void main(String[] args) {
        Path myFilePath = Paths.get("myNewFile.txt");
        try {
            LoremIpsum ipsum = new LoremIpsum();
            String fileParagraph = ipsum.getParagraphs(1);
            Files.write(myFilePath, Collections.singleton(fileParagraph));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<String> fileLine = Files.readAllLines(myFilePath);
            int numberOfLoremOccurrences = 0;
            Pattern p = Pattern.compile("lorem");
            for (String s : fileLine) {
                s = s.toLowerCase();
                Matcher m = p.matcher(s);
                while (m.find()) {
                    numberOfLoremOccurrences++;
                }
            }
            System.out.println("numberOfLoremOccurrences = " + numberOfLoremOccurrences);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
