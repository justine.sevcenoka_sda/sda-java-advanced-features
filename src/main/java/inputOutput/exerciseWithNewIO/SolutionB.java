/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
b) *Count special signs (like comma, dot, spaces).
 */
package inputOutput.exerciseWithNewIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class SolutionB {

    public static void main(String[] args) {
        Path myFilePath = Paths.get("myNewFile.txt");
        try {
            LoremIpsum ipsum = new LoremIpsum();
            String fileParagraph = ipsum.getParagraphs(1);
            Files.write(myFilePath, Collections.singleton(fileParagraph));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            List<String> fileLine = Files.readAllLines(myFilePath);
            int specialSignsCount = 0;
            for (String s : fileLine) {
                s = s.replaceAll("[a-zA-Z]", "");
                specialSignsCount += s.length();
            }
            System.out.println("specialSignsCount = " + specialSignsCount);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
