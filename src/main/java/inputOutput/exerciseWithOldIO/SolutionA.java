/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
a) Count words.
 */
package inputOutput.exerciseWithOldIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.*;
import java.util.Scanner;

public class SolutionA {

    public static void main(String[] args) {
        File myFile = new File("myFile.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile))) {
            LoremIpsum ipsum = new LoremIpsum();
            String fileLine = ipsum.getParagraphs(1);
            bufferedWriter.write(fileLine);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Scanner sc = new Scanner(new FileInputStream(myFile))) {
            int wordCount = 0;
            while (sc.hasNext()) {
                sc.next();
                wordCount++;
            }
            System.out.println("wordCount = " + wordCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
