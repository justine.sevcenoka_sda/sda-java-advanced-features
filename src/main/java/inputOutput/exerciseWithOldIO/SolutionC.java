/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
*Select one word and print it’s number of occurences.
 */
package inputOutput.exerciseWithOldIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionC {

    public static void main(String[] args) {
        File myFile = new File("myFile.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile))) {
            LoremIpsum ipsum = new LoremIpsum();
            String fileLine = ipsum.getParagraphs(1);
            bufferedWriter.write(fileLine);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(myFile))) {
            String fileLine;
            int numberOfLoremOccurrences = 0;
            while ((fileLine = bufferedReader.readLine()) != null) {
                fileLine = fileLine.toLowerCase();
                Pattern p = Pattern.compile("lorem");
                Matcher m = p.matcher(fileLine);
                while (m.find()) {
                    numberOfLoremOccurrences++;
                }
            }
            System.out.println("numberOfLoremOccurrences = " + numberOfLoremOccurrences);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
