/*
Create a file with a „lorem ipsum” paragraph within – it can be done by copy-pasting existing paragraph or
generating it dynamically using Java library. Read that file.
b) *Count special signs (like comma, dot, spaces).
 */
package inputOutput.exerciseWithOldIO;

import de.svenjacobs.loremipsum.LoremIpsum;

import java.io.*;

public class SolutionB {

    public static void main(String[] args) {
        File myFile = new File("myFile.txt");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(myFile))) {
            LoremIpsum ipsum = new LoremIpsum();
            String fileLine = ipsum.getParagraphs(1);
            bufferedWriter.write(fileLine);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(myFile))) {
            int specialSignsCount = 0;
            String fileLine;
            while ((fileLine = bufferedReader.readLine()) != null) {
                fileLine = fileLine.replaceAll("[a-zA-Z]", "");
                specialSignsCount += fileLine.length();

            }
            System.out.println("specialSignsCount = " + specialSignsCount);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
