package exceptions.exercise1;
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {

        try (Scanner sc = new Scanner(System.in)) {
            System.out.print("Enter any number: ");
            String input = sc.nextLine();

            try {
                int intInput = Integer.parseInt(input);
                System.out.println("int -> " + intInput);
            } catch (NumberFormatException e) {
                double doubleInput = Double.parseDouble(input);
                System.out.println("double -> " + doubleInput);
            }
        } catch (NumberFormatException e) {
            System.out.println("Hey! That’s not a value! Try once more.");
        } catch (Exception e) {
            System.out.println("Unknown exception occurred");
        }
    }
}
