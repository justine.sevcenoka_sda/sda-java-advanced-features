package exceptions.exercise1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SolutionB {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter any number: ");
        if (sc.hasNextInt()) {
            System.out.println("int -> " + sc.nextInt());
        } else if (sc.hasNextDouble()) {
            System.out.println("double -> " + sc.nextDouble());
        } else throw new InputMismatchException("Hey! That’s not a value! Try once more.");
    }
}
