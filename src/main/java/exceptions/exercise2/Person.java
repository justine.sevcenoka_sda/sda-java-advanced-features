package exceptions.exercise2;

public class Person {
    private String name;
    private String surname;
    private String middleName;

    public Person() {
    }

    public String getMiddleName() {
        return middleName;
    }

    public void fillNames(String nameSurname) throws SurnameNotProvidedException {
        String[] separatedNameSurname = nameSurname.split(" ");

        if (separatedNameSurname.length == 1) {
            throw new SurnameNotProvidedException("Surname not provided");
        } else if (separatedNameSurname.length == 2) {
            this.name = separatedNameSurname[0];
            this.surname = separatedNameSurname[1];

        } else if (separatedNameSurname.length == 3) {
            this.name = separatedNameSurname[0];
            this.middleName = separatedNameSurname[1];
            this.surname = separatedNameSurname[2];
        }
    }


}
