package exceptions.exercise2;

public class SurnameNotProvidedException extends Exception {
    public SurnameNotProvidedException(String message) {
        super(message);
    }
}
