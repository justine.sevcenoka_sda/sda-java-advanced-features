/* What do you think about raising an exception: when should we raise an exception and when return Null on method „failure”?
Try to implement one method for each situation. */

package exceptions.exercise2;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws SurnameNotProvidedException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter your name and surname separated by space: ");
        String nameSurname = sc.nextLine();
        sc.close();

        String[] separatedNameSurname = separateNameAndSurname(nameSurname);

        System.out.println("Surname is provided: " + isSurnameProvided(separatedNameSurname));

        String middleName = getMiddleName(separatedNameSurname);
        if (middleName != null) {
            System.out.println("Middle name is: " + getMiddleName(separatedNameSurname));
        }
    }

    private static String[] separateNameAndSurname(String nameSurname) {
        return nameSurname.split(" ");
    }


    private static boolean isSurnameProvided(String[] separatedNameSurname) throws SurnameNotProvidedException {
        if (separatedNameSurname.length > 1) {
            return true;
        } else {
            throw new SurnameNotProvidedException("Surname not provided");
        }
    }

    //piemēra vienkāršības pēc pieņemam, ka tiek norādīti tikai max 3 ar space atdalīti vārdi
    private static String getMiddleName(String[] separatedNameSurname) {
        if (separatedNameSurname.length > 2) {
            return separatedNameSurname[1];
        } else
            return null;
    }


}
