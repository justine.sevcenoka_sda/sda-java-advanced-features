/* What do you think about raising an exception: when should we raise an exception and when return Null on method „failure”?
Try to implement one method for each situation.
Piemēra vienkāršības pēc pieņemam, ka tiek norādīti tikai max 3 ar space atdalīti vārdi */

package exceptions.exercise2;

import java.util.Scanner;

public class SolutionB {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter your name and surname separated by space: ");
        String nameSurname = sc.nextLine();
        sc.close();

        Person person = new Person();
        try {
            person.fillNames(nameSurname);
        } catch (SurnameNotProvidedException e) {
            System.err.println(e.getMessage());
        }

        String middleName = person.getMiddleName();
        if (middleName != null) {
            System.out.println("Middle name is: " + middleName);
        }

    }
}
