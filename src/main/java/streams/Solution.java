//1. Use streams, for a given lists:
//- [„John”, „Sarah”, „Mark”, „Tyla”, „Ellisha”, „Eamonn”]
// - [1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50]
package streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Solution {
    public static void main(String[] args) {
//a) Sort the list.
        List<String> stringList = Arrays.asList("John", "Sarah", "Mark", "Tyla", "Ellisha", "Eamonn");
        stringList = stringList.stream()
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList());
        System.out.println(stringList.toString());

        List<Integer> integerList = Arrays.asList(1, 4, 2346, 123, 76, 11, 0, 0, 62, 23, 50);
        integerList = integerList.stream()
                .sorted(Integer::compareTo)
                .collect(Collectors.toList());
        System.out.println(integerList);
//b) Print only those names, that start with „E” letter.
        stringList.stream()
                .filter(n -> n.startsWith("E"))
                .map(n -> n + " ")
                .forEach(System.out::print);
        printLine();
//c) Print values greater than 30 and lower than 200.
        List<Integer> greater30Lower200 = integerList.stream()
                .filter(n -> n > 30 && n < 200)
                .collect(Collectors.toList());
        System.out.println(greater30Lower200);
//d) Print names uppercase.
        stringList.stream()
                .map(String::toUpperCase)
                .map(n -> n + " ")
                .forEach(System.out::print);
        printLine();
//e) Remove first and last letter, sort and print names.
        String removedFirstLastSorted = stringList.stream()
                .map(s -> s.substring(1, s.length() - 1))
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.joining(" "));
        System.out.println(removedFirstLastSorted);
//f) *Sort backwards by implementing reverse Comparator and using lambda expression.
// f - 1
        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return -o1.compareTo(o2);
            }
        };

        stringList.stream()
                .sorted(comparator);

// f - 2
        stringList.stream()
                .sorted(Comparator.reverseOrder())
                .map(n -> n + " ")
                .forEach(System.out::print);

        printLine();

        List<Integer> backwardSorted = integerList.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
        System.out.println(backwardSorted);
    }

    private static void printLine() {
        System.out.println();
    }
}
