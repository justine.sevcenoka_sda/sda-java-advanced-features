package streams.codingGameTasks;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class UpperCase {
    public static void main(String[] args) {
        System.out.println(getTotalNumberOfLettersOfNamesLongerThanFive("myname", "somename"));
    }

    public static Collection<String> mapToUppercase(String... names) {
        return Arrays.stream(names).
                map(String::toUpperCase).
                collect(toList());
    }

    public static int getTotalNumberOfLettersOfNamesLongerThanFive(String... names) {
        return Arrays.stream(names).
                filter(s -> s.length() > 5).
                mapToInt(String::length).sum();

    }

    public static List<String> transform(List<List<String>> collection) {
        return collection.stream().
                flatMap(Collection::stream).
                collect(toList());
    }

    public static int calculate(List<Integer> numbers) {

        int total = 0;
        for (int number : numbers) {
            total += number;
        }
        return total;
    }


}